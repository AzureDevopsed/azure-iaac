# Azure-IaaC

## Add your files

- [ ] [Clone](https://gitlab.com/AzureDevopsed/azure-iaac.git) files

```
cd existing_repo
git remote add origin https://gitlab.com/AzureDevopsed/azure-iaac.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Portable azure cli](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli-windows?tabs=zip)

## Collaborate with your team

- [ ] [Batch script use az cli command which requrie installation]
- [ ] [Batch file should replace 'az' with 'az.cmd' to use portable azcli option]ge_when_pipeline_succeeds.html)


## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
