@echo off
set username=name
set /P templateName="Enter template directory path and template name with extension: "
set /P azclipath="Enter azcli path : "
set /P regr="Enter ResourceGroupName to deploy on: "

if exist "%azclipath%\az.cmd" (
    cd "%azclipath%"
    if %ERRORLEVEL% == 0 (
        echo Valid directory %azclipath%, proceeding to the next step...
		echo %CD%
    ) else (
        echo An error occurred for azcli: %ERRORLEVEL%
        pause
        exit
    )
) else (
    echo Azcli directory "%azclipath%" or azcli does not exist.
    pause
    exit
)

az account show | find "%username%"
if %ERRORLEVEL% == 0 (
   echo Already logged in, proceeding to the next step...
) else (
    echo An error occurred during login: %ERRORLEVEL%
    pause
    exit 
)

az group list --output table | find "%regr%"
if %ERRORLEVEL% == 0 (
    echo "Resource group exist, continuing"
    exit
) else (
    echo "Resources group %regr%  doesnt exist, please contact edmin"
    pause
    exist
)

az resource list --resource-group %regr% --output table | find "Microsoft"
if %ERRORLEVEL% == 0 (
    echo "Resources exist, please contact edmin"
    pause
    exit
) else (
    echo Attempting resources from %templateName%
	az deployment group create --resource-group %regr% --template-file %templateName%
	if %ERRORLEVEL% equ 0 (
    	echo "Deployment successful"
	) else (
		echo Deployment failed
	)
)
echo bye bye
pause